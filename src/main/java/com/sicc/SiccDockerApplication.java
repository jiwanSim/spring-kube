package com.sicc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SiccDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiccDockerApplication.class, args);
	}

}
