FROM openjdk:8-jdk-alpine
FROM sonarqube
MAINTAINER 'jiwan8985' 'jiwan8985@gmail.com'
VOLUME /tmp
#ENV SERVER_ENV ${SERVER_ENV}
ADD sonar-l10n-zh-plugin-1.21.jar /opt/sonarqube/extensions/plugins/
ENV JAVA_OPTS="-XX:PermSize=1024m -XX:MaxPermSize=512m -Xmx4g -Xms2g"
COPY ./app.jar app.jar
#ENTRYPOINT ["java", "-Dspring.profiles.active=${SERVER_ENV}", "-jar","/app.jar"]